package org.ssldev.crateexporter.gui;

import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.ssldev.core.utils.Logger;
import org.ssldev.crateexporter.CrateExporterMain;
import org.ssldev.crateexporter.gui.CrateExporterGUI.TrackGui;
import org.ssldev.crateexporter.gui.CrateTreeModel.Crate;
import org.ssldev.crateexporter.services.CrateExporterGuiService;
import org.ssldev.crateexporter.utils.CrateAudioFile;
import org.ssldev.crateexporter.utils.IFormatter;
import org.ssldev.crateexporter.utils.IFormatter.FormatTypes;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.DirectoryChooser;
import javafx.util.Duration;

public class CrateExporterMainController implements Initializable{
	
	@FXML private TreeView<Crate> cratesTreeView;
	@FXML private TableView<TrackGui> centerTable;
	@FXML private TableColumn<TrackGui, Integer> numberCol;
	@FXML private TableColumn<TrackGui, String> songTitleCol;
	@FXML private TableColumn<TrackGui, String>  artistCol;
	@FXML private TableColumn<TrackGui, String>  albumCol;
	@FXML private Label statusLbl;
	@FXML private Label versionLbl;

	// BOTTOM
	@FXML private Button selectOutputDirBtn;
	@FXML private Label selectedOutputDirLbl;
	// select serato crates directory button
	@FXML private Button selectCratesDirBtn;
	// absolute path of serato crates directory
	@FXML private Label selectedCrateDirLbl;
	@FXML private Label exportAsLbl;
	@FXML private ChoiceBox<String> outputFormatsChoices;
	@FXML private Button startBtn;
	@FXML private Button openBtn;
	
	private CrateExporterGUI gui;
	
	private CrateTreeModel cratesTree;
	
	private ObservableList<String> exportFormats = FXCollections.observableList(new ArrayList<>());
	
	/**
	 * export directory selected
	 */
	private File selectedDirectory;
	
	/**
	 * last crate selected
	 */
	private Crate selectedCrate;
	
	/**
	 * absolute path of export dir selected 
	 */
	private String resultPath;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		artistCol.setCellValueFactory(new PropertyValueFactory<TrackGui, String>("artist"));
		numberCol.setCellValueFactory(new PropertyValueFactory<TrackGui, Integer>("num"));
		songTitleCol.setCellValueFactory(new PropertyValueFactory<TrackGui, String>("title"));
    	albumCol.setCellValueFactory(new PropertyValueFactory<TrackGui, String>("album"));
    	versionLbl.setText(CrateExporterMain.VERSION);
    	centerTable.setPlaceholder(new Label("No Crate Loaded"));
    	
    	// add crate export formats
		for (FormatTypes type : IFormatter.FormatTypes.values()) {
			exportFormats.add(type.extension);
		}
		outputFormatsChoices.setItems(exportFormats);
		outputFormatsChoices.getSelectionModel().selectFirst();
    	
    	// set row to red if file was not not found or properly decoded
    	centerTable.setRowFactory(tv -> new TableRow<>() {
    		@Override
    		public void updateItem(TrackGui item, boolean empty) {
    			super.updateItem(item, empty);
    			if(item == null) {
    				setStyle("");
    			}
    			else if(item.errorEncounteredOnCreate) {
    				this.setStyle("-fx-background-color: red;");
    			}
    			else {
    				this.setStyle("");
    			}
    		};
    	});
    	
    	selectOutputDirBtn.setOnAction(this::onExportTo);
    	selectCratesDirBtn.setOnAction(this::onCrateDirSelected);
    	startBtn.setOnAction(this::onStartExport);
    	openBtn.setOnAction(this::onOpen);
    	
    	// set what happens when user clicks on crate 
    	cratesTreeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
	        if(newValue != null && newValue != oldValue){
	            onCrateSelected(newValue.getValue());
	     }
    	});
    	
    	//TODO finish styling..
    	cratesTreeView.getStyleClass().add("my-tree-view");
    	
    	cratesTree = new CrateTreeModel(cratesTreeView);
    	
    	appController().onControllerInitialized(this);
	}
	
	// export-to was pressed
	private void onExportTo(ActionEvent event) {
		DirectoryChooser directoryChooser = new DirectoryChooser();
		selectedDirectory = directoryChooser.showDialog(gui.stage);
		selectedOutputDirLbl.setText(selectedDirectory == null? "" : selectedDirectory.getAbsolutePath());
		
		toggleStartButton();
	}
	
	// 'select crates dir' button was pressed
	private void onCrateDirSelected(ActionEvent event) {
		DirectoryChooser directoryChooser = new DirectoryChooser();
		selectedDirectory = directoryChooser.showDialog(gui.stage);
		if(null != selectedDirectory) {
			setCratesDirSelected(selectedDirectory.getAbsolutePath());
			appController().onSeratoCratesDirSelected(selectedDirectory);
			setCratesTreeViewRootExpanded(true);
		}
	}
	
	// crate was selected by user
	private void onCrateSelected(Crate crate) {
		if(crate.file.isDirectory()) {
			Logger.debug(this, "directory selected.. not converting");
			return;
		}
		else if(cratesTree.isParent(crate)) {
			Logger.debug(this, "crate with subcrates selected...");
			onCratesSelected(crate, cratesTree.getChildren(crate));
		}
		else {
			disableCratesTree(true);  // disabling the tree until the table updates
			selectedCrate = crate;
			toggleStartButton();
			appController().onCrateSelected(crate);
		}
	}

	// display content of parent crate and all of its children 
	private void onCratesSelected(Crate parent, List<Crate> children) {
		disableCratesTree(true);  // disabling the tree until the table updates
		selectedCrate = parent;
		toggleStartButton();
		appController().onCratesSelected(parent, children);
	}

	// disable or enable the tree view (mostly to prevent consecutive crate requests)
	private void disableCratesTree(boolean isDisabled) {
		cratesTreeView.setDisable(isDisabled);
	}

	@SuppressWarnings("exports")
	public void displayCrate(String crateName, String content, List<CrateAudioFile> mp3s) {
		CrateExporterGuiContext.loadedContent(content);
		CrateExporterGuiContext.loadedCrateName(crateName);

		disableCratesTree(false);  // re-enable the tree for user to select other crates
		
		Platform.runLater(()-> populateTable(mp3s, crateName));
	}
	
	/**
	 * updates the bottom status label with the given status
	 * @param status to display briefly
	 */
	public void  updateStatus(String status) {
		Platform.runLater(() -> {
			statusLbl.setText(status);
			
			 FadeTransition fadeTransition = new FadeTransition(Duration.seconds(5), statusLbl);
			 fadeTransition.setFromValue(1.0);
			 fadeTransition.setToValue(0.0);
			 fadeTransition.setCycleCount(1);
			 fadeTransition.play();
		});
	}
	
	// toggles the start button if it can be enabled
	private void toggleStartButton() {
		disableStartButton( selectedCrate == null || selectedDirectory == null );
	}
	
	/**
	 * populate the crate tree with all crates contained in the given root directory
	 * @param dir containing serato .crate files
	 */
	public void setTreeRoot(File dir) {
		cratesTree.createTree(dir);
	}
	
	// populate the table with the given crate contentes
	private void populateTable(List<CrateAudioFile> mp3s, String crateName) {
		centerTable.getItems().clear();
		int trackNum = 1;  
		for(CrateAudioFile m : mp3s) {
			centerTable.getItems().add(new TrackGui(m, trackNum++));
		}
		
		setRootTitle(crateName);
	}
	
	// open the exported file
	private void onOpen(ActionEvent evt) {
		File result = new File(resultPath);
		if (!result.exists()) {
			Logger.error(this, result + " does not exist");
		} 
		else {
			try {
				Desktop.getDesktop().open(result);
			} catch (IOException e) {
				Logger.error(this, "failed opening " + result);
				e.printStackTrace();
			}
		}
		
	}
	
	// start export clicked
	private void onStartExport(ActionEvent evt ) 
	{
		if(selectedDirectory == null){
		     //No Directory selected
			Logger.info(this, "output directory not yet selected");
		}
		else if (selectedCrate == null) {
			Logger.info(this, "crate not yet selected");
		}
		else{
			resultPath = selectedDirectory.getAbsolutePath()  + File.separator
					+ CrateExporterGuiContext.getCrateName()
											 .replaceAll(".crate",
											             getSelectedFormatType().extension);
			
			resultPath = resultPath.replaceAll("/\\.", "/"); // fix crate names that start with '.'

			Logger.info(this, System.lineSeparator() +
							  "Exporting: ["+CrateExporterGuiContext.getCrateName()+"]"+System.lineSeparator() +
							  "To dir   : ["+selectedDirectory.getAbsolutePath()+"]"   +System.lineSeparator() +
							  "In format: ["+getSelectedFormatType()+"]"
					);
			

		     createFileIfDoesntExist(resultPath);
		     
		     updateStatus("wrote crate to "+ resultPath + "...");
		     
		     String content = getSelectedFormatType().equals(FormatTypes.TEXT) ?
		    		 CrateExporterGuiContext.getContentAsText() :
		    			 CrateExporterGuiContext.getContentAsM3u();
		     
    		 appController().writeToFile(resultPath, content);
		    		 
		     
		     disableOpenButton(false);
		}
		
	}
	
	//TODO move to core..
	private void createFileIfDoesntExist(String txtFileAbsPath) {
		try {
			Files.createFile(Paths.get(txtFileAbsPath));
		} catch (FileAlreadyExistsException e) {
			Logger.debug(this, "no need to create file. it already exists: "+txtFileAbsPath);
			clearFileContents(txtFileAbsPath);
		} catch (IOException e) {
			throw new RuntimeException("unable to create file",e);
		}
	}
	
	private void clearFileContents(String filePath) {
		try {
			new PrintWriter(filePath).close();
		} catch (FileNotFoundException e) {
			Logger.warn(this, "failed to clear out contents of file: "+filePath);
		}
	}
	
	private void setRootTitle(String title) {
		gui.stage.setTitle(title);
	}
	
	protected FormatTypes getSelectedFormatType() {
		int selectedIndex = outputFormatsChoices.getSelectionModel().getSelectedIndex();
		return FormatTypes.values()[selectedIndex];
	}
	
	private void disableStartButton(boolean isDisable) {
		startBtn.setDisable(isDisable);
		disableOpenButton(true);
	}
	private void disableOpenButton(boolean isDisable) {
		openBtn.setDisable(isDisable);
	}
	

	public void setM3uContent(String contentAsM3u) {
		CrateExporterGuiContext.setM3uContent(contentAsM3u);
	}

	
	public void setGui(CrateExporterGUI gui) {
		this.gui = gui;
	}
	
	public List<Crate> getAllCrates() {
		return cratesTree.getAllCrates();
	}
	
	private CrateExporterGuiService appController() {
		return CrateExporterGuiService.instance();
	}

	public void setCratesDirSelected(String dirPath) {
		selectedCrateDirLbl.setText(dirPath);
		
		setCratesTreeViewRootExpanded(true);
	}
	
	private void setCratesTreeViewRootExpanded(boolean isExpanded) {
		TreeItem<Crate> root = cratesTreeView.getRoot();
		if(null != root) {
			root.setExpanded(isExpanded);
		}
	}

}
