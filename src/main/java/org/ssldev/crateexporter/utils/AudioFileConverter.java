package org.ssldev.crateexporter.utils;

import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.ssldev.core.utils.Logger;


public class AudioFileConverter {
	
	public static CrateAudioFile extractTaggedInfo(File f) {
		Logger.debug(AudioFileConverter.class, "attempting to extract " + f);
		
		File fileToExtract = f;
		
		if(!fileToExtract.exists()) {
			
			fileToExtract = searchAllRoots(fileToExtract);
			if(!fileToExtract.exists()) {
				Logger.warn(AudioFileConverter.class, "could not find the following file: " + f);
				return failedConversion(f);
			}
		}
		
		try {
			AudioFile af = AudioFileIO.read(fileToExtract);
			Tag tag = af.getTag();
			CrateAudioFile mp3 = new CrateAudioFile()
					.setArtist(nonNull(tag.getFirst(FieldKey.ARTIST)))
					.setTitle(nonNull(tag.getFirst(FieldKey.TITLE)))
					.setAlbum(nonNull(tag.getFirst(FieldKey.ALBUM)))
					.setKey(nonNull(tag.getFirst(FieldKey.KEY)))
					.setComposer(nonNull(tag.getFirst(FieldKey.COMPOSER)))
					.setF(fileToExtract)
					.setLastModified(fileToExtract.lastModified())
					.setLengthInSeconds(af.getAudioHeader().getTrackLength())
					.setSize(fileToExtract.length());
			return mp3;
		} catch (Throwable e) {
			Logger.error(AudioFileConverter.class, "encountered exception attempting to decode ["+fileToExtract+"]", e);
			return failedConversion(fileToExtract);
		}
	}

	// FIXME
	private static File searchAllRoots(File f) {
		File[] roots = File.listRoots();
		for(File root : roots) {
			
			Optional<File> fileOnAnotherDrive =  getFile(root, f);
			
			if(fileOnAnotherDrive.isPresent()) {
				Logger.debug(AudioFileConverter.class, "found ["+f.getName()+"] on another drive ["+root.getAbsolutePath()+"]");
				return fileOnAnotherDrive.get();
			}
		}
		
		//may work on OSX if external HD is connected as a 'volume'
		File volumes = new File("/Volumes");
		if(volumes.exists()) {
			for(File volume : volumes.listFiles()) {
				File fileOnAnotherDrive = Paths.get(volume.getAbsolutePath(), f.getPath()).toFile();
				if(fileOnAnotherDrive.exists()) {
					Logger.debug(AudioFileConverter.class, "found ["+f.getName()+"] on another drive ["+volume.getAbsolutePath()+"]");
					return fileOnAnotherDrive;
				}
			}
		}
		
		return f;
	}

	// return file formed by the path of the root + relative path of the other given file
	private static Optional<File> getFile(File root, File f) {
		try {
			
			Path relativePath = Paths.get(root.getAbsolutePath(), f.getPath());
			File relativeFile = relativePath.toFile();
			return  relativeFile.exists() ? Optional.of(relativeFile) : Optional.empty();
			
		} catch(InvalidPathException e) {
			Logger.error(AudioFileConverter.class, "encountered error searching for ["+f.getName()+"] on ["+root.getAbsolutePath()+"]\n"
					+ e.getMessage());
			return Optional.empty();
		}
	}

	private static CrateAudioFile failedConversion(File f) {
		return new CrateAudioFile().setF(f).setErrorOnCreate(true);
	}

	private static String nonNull(String s) {
		return s == null ? "" : s;
	}
	
}
