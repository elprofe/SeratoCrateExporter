package org.ssldev.crateexporter.messages;

import java.io.File;

import org.ssldev.core.messages.Message;
import org.ssldev.core.utils.Validate;

public class ExtractedMp3Message extends Message {

	private File mp3File;

	public ExtractedMp3Message(File mp3File) {
		Validate.notNull(mp3File, "mp3 file cannot be null");
		Validate.isTrue(mp3File.exists(), "mp3 file does not exist: " + mp3File.getAbsolutePath());
		this.mp3File = mp3File;
	}

	public File getMp3File() {
		return mp3File;
	}

}
