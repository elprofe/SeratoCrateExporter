package org.ssldev.crateexporter.services;

import static java.util.stream.Collectors.toList;
import static org.ssldev.crateexporter.utils.AudioFileConverter.extractTaggedInfo;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ssldev.api.messages.CrateConvertRequestMessage;
import org.ssldev.api.messages.CrateConvertResponseMessage;
import org.ssldev.core.mgmt.EventHub;
import org.ssldev.core.services.Service;
import org.ssldev.core.utils.Logger;
import org.ssldev.crateexporter.messages.CrateAsM3uMessage;
import org.ssldev.crateexporter.messages.CrateAsTextMessage;
import org.ssldev.crateexporter.messages.CrateToTextMessage;
import org.ssldev.crateexporter.messages.ExtractedMp3Message;
import org.ssldev.crateexporter.utils.CrateAudioFile;
import org.ssldev.crateexporter.utils.IFormatter;
import org.ssldev.crateexporter.utils.M3uFormatter;
import org.ssldev.crateexporter.utils.TextFormatter;

/**
 */
public class CrateToTextService extends Service{
	// crate name to text file to write to
	private Map<String, String> crateToTxtMap = new HashMap<>();
	
	private IFormatter textFormatter = new TextFormatter();
	private IFormatter m3uFormatter = new M3uFormatter();
	
	public CrateToTextService(EventHub hub) {
		super(hub);
		
		// TODO: message registration should happen in init. but SSL API currently inits the hub before 
		// the registered services have a chance to be initialized...
	}
	
	@Override
	public void init() {
		hub.register(CrateToTextMessage.class, this::handle);
		hub.register(CrateConvertResponseMessage.class, this::handle);
	}

	// convert crate to POJO
	private void handle(CrateToTextMessage msg) {
		Logger.debug(this, "requesting crate-to-pojo convert for " + msg);
		
		if(msg.txtFileOutput == null) {
			crateToTxtMap.put(msg.crateFile.getName(), null);
		}
		else {
			crateToTxtMap.put(msg.crateFile.getName(), msg.txtFileOutput.getAbsolutePath());
		}
		
		CrateConvertRequestMessage c = new CrateConvertRequestMessage(msg.crateFile , msg.crateName);
		hub.add(c);
	}
	
	// handle converted crate
	private void handle(CrateConvertResponseMessage msg) {
		Logger.debug(this, "got back " + msg);
		
		String crateName = msg.crateName + ".crate";
		
		List<CrateAudioFile> mp3s = msg.crateTracksPaths.stream()
			.map(path -> extractTaggedInfo(new File(path)))
			.peek(this::publishStatus)  // update the display status..
			.collect(toList());
		
		String contentAsText = textFormatter.format(msg.displayedColumns, mp3s);
		
		hub.add(new CrateAsTextMessage(msg.crateFile, crateName, contentAsText, mp3s));
		
		String contentAsM3u = m3uFormatter.format(msg.displayedColumns, mp3s);
		hub.add(new CrateAsM3uMessage(crateName, contentAsM3u, mp3s));
	}
	
	private void publishStatus(CrateAudioFile m) {
		if(null == m || m.getF() == null || !m.getF().exists() || m.encounteredErrorOnCreate()) {
			Logger.warn(this, "unable to convert " + m.getF());
		}
		else {
			hub.add(new ExtractedMp3Message(m.getF()));
		}
	}

}
